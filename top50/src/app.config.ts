export default {
  pages: [
    'pages/index/index',
    'pages/movie/movie'
    
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  "subPackage": [
    {
        "root": "packageA",
        "pages": [
          'pages/index/index',
          'pages/movie/movie'
        ]
    }
],
"routes": [
    {
        // 投放入口，scheme中的path
        "path": "index", 
        // 真实的物理存储路径
        "page": "pages/index/index" 
    },
    {
        "path": "movie",
        "page": "pages/movie/movie"
    }
  ]
}
