import Vue from 'vue'
import axios from 'axios'


/* 2、 设置全局过滤器 */
Vue.filter("handleStr",function(val){
    if(val.length>3){
      val=val.slice(0,3)+"..."
    }
    return val
  })
  /* 3、设置axios */
  axios.defaults.baseURL = "http://47.108.197.28:4000";
  Vue.prototype.$http = axios;

  /* 4、配置全局组件 */
  import TextItem from '../components/TextItem.vue'
  Vue.component("TextItem",TextItem)